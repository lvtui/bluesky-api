package com.bluesky.admin;

import com.bluesky.common.config.AppConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Import;

/**
 * Admin启动类
 *
 * @author Kevin
 */
@SpringBootApplication(scanBasePackages = "com.bluesky", exclude = {DataSourceAutoConfiguration.class})
@Import(cn.hutool.extra.spring.SpringUtil.class)
public class AdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(AdminApplication.class, args);
        System.out.println(String.format("============ %s API 启动成功 ============", AppConfig.getName().toUpperCase()));
    }

}
