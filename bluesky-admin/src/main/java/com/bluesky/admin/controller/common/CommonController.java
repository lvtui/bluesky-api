package com.bluesky.admin.controller.common;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.StrUtil;
import com.bluesky.common.config.AppConfig;
import com.bluesky.common.exception.CustomException;
import com.bluesky.common.result.R;
import com.bluesky.common.result.ResultCode;
import com.bluesky.oss.strategy.FileStorageStrategy;
import com.bluesky.oss.strategy.context.FileStorageContext;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Objects;


/**
 * <p>
 * 通用common 前端控制器
 * </p>
 *
 * @author Kevin
 */
@RestController
@RequestMapping("/common")
@Slf4j
public class CommonController {

    @Resource
    private FileStorageContext fileStorageContext;


    /**
     * 下载文件名重新编码
     *
     * @param response     响应对象
     * @param realFileName 真实文件名
     * @return
     */
    public static void setAttachmentResponseHeader(HttpServletResponse response, String realFileName) throws UnsupportedEncodingException {
        String percentEncodedFileName = percentEncode(realFileName);
        StringBuilder contentDispositionValue = new StringBuilder();
        contentDispositionValue.append("attachment; filename=")
                .append(percentEncodedFileName)
                .append(";")
                .append("filename*=")
                .append("utf-8''")
                .append(percentEncodedFileName);

        response.setHeader("Content-disposition", contentDispositionValue.toString());
    }

    /**
     * 百分号编码工具方法
     *
     * @param s 需要百分号编码的字符串
     * @return 百分号编码后的字符串
     */
    public static String percentEncode(String s) throws UnsupportedEncodingException {
        String encode = URLEncoder.encode(s, StandardCharsets.UTF_8.toString());
        return encode.replaceAll("\\+", "%20");
    }

    @PostMapping("/upload")
    @SneakyThrows
    public R upload(MultipartFile file, HttpServletRequest request) {
        String bizPath = request.getParameter("bizPath");
        if (StrUtil.isBlank(bizPath)) {
            throw new CustomException(ResultCode.PARAM_MISS.getMessage());
        }
        if (Objects.isNull(file)) {
            throw new CustomException(ResultCode.PARAM_MISS.getMessage());
        }
        FileStorageStrategy fileStorageStrategy = fileStorageContext.getContext();
        String url = fileStorageStrategy.upload(file, bizPath);
        return R.success(url);
    }

    @GetMapping("/download")
    @SneakyThrows
    public void download(@RequestParam String filepath, HttpServletResponse response) {
        response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
        // 文件名称转换，用户列表_uuid.xlsx -> 用户列表_1637469159968.xlsx
        String filepathConvert = StrUtil.subBefore(filepath, "_", true)
                + "_" + System.currentTimeMillis() + "."
                + StrUtil.subAfter(filepath, ".", true);
        setAttachmentResponseHeader(response, FileUtil.getName(filepathConvert));
        FileUtil.writeToStream(AppConfig.getUploadDir() + filepath, response.getOutputStream());
    }

}
