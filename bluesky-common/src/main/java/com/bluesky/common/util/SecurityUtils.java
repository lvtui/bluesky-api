package com.bluesky.common.util;

import com.bluesky.common.bo.LoginUser;
import com.bluesky.common.bo.SysUserBO;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Objects;

/**
 * 安全服务工具类
 *
 * @author Kevin
 */
public class SecurityUtils {
    /**
     * 获取用户账户
     **/
    public static String getUsername() {
        try {
            return getLoginUser().getUsername();
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 获取系统用户
     **/
    public static SysUserBO getSysUser() {
        return getLoginUser().getSysUser();
    }

    /**
     * 获取用户
     **/
    public static LoginUser getLoginUser() {
        try {
            return (LoginUser) getAuthentication().getPrincipal();
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 获取租户ID
     **/
    public static Long getTenantId() {
        return getLoginUser().getTenantId();
    }

    /**
     * 获取租户编码
     **/
    public static String getTenantCode() {
        return getLoginUser().getTenantCode();
    }

    /**
     * 是否是超级管理员
     **/
    public static Boolean isSuperAdmin() {
        return isSuperAdmin(getSysUser().getId());
    }

    /**
     * 是否是超级管理员
     **/
    public static Boolean isSuperAdmin(Long userId) {
        return Objects.nonNull(userId) && userId == 1L;
    }

    /**
     * 获取Authentication
     */
    public static Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    /**
     * 生成BCryptPasswordEncoder密码
     *
     * @param password 密码
     * @return 加密字符串
     */
    public static String encryptPassword(String password) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.encode(password);
    }

    /**
     * 判断密码是否相同
     *
     * @param rawPassword     真实密码
     * @param encodedPassword 加密后字符
     * @return 结果
     */
    public static boolean matchesPassword(String rawPassword, String encodedPassword) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.matches(rawPassword, encodedPassword);
    }

}
