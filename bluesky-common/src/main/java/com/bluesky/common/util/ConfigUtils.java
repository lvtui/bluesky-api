package com.bluesky.common.util;

import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.spring.SpringUtil;
import com.bluesky.common.Constants;
import com.bluesky.common.redis.RedisUtils;

import java.util.Collection;
import java.util.Objects;

/**
 * 系统参数缓存工具类
 *
 * @author Kevin
 */
public class ConfigUtils {

    public static String getCacheKey(String key) {
        return String.format(Constants.SYS_CONFIG_TPL, key);
    }

    public static String getConfigValue(String key) {
        if (StrUtil.isBlank(key)) {
            return "";
        }
        Object obj = SpringUtil.getBean(RedisUtils.class).get(getCacheKey(key));
        return Objects.nonNull(obj) ? obj.toString() : "";
    }

    public static void setConfigCache(String key, String value) {
        if (StrUtil.isBlank(key) || StrUtil.isBlank(value)) {
            return;
        }
        SpringUtil.getBean(RedisUtils.class).set(getCacheKey(key), value);
    }

    public static void clearConfigCache() {
        Collection<String> keys = SpringUtil.getBean(RedisUtils.class).keys(getCacheKey("*"));
        SpringUtil.getBean(RedisUtils.class).del(keys.toArray(new String[0]));
    }

}
