package com.bluesky.common.util;

import com.bluesky.common.config.MinioConfig;
import com.bluesky.common.exception.CustomException;
import io.minio.BucketExistsArgs;
import io.minio.MakeBucketArgs;
import io.minio.MinioClient;
import io.minio.PutObjectArgs;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.InputStream;

/**
 * Minio工具类
 *
 * @author Kevin
 */
@Component
public class MinioUtils {

    @Resource
    MinioConfig minioConfig;

    @Resource
    MinioClient minioClient;

    @Bean
    private MinioClient minioClient() {
        MinioClient minioClient = MinioClient.builder()
                .endpoint(minioConfig.getEndpoint())
                .credentials(minioConfig.getAccessKey(), minioConfig.getSecretKey())
                .build();
        return minioClient;
    }

    public String uploadObject(InputStream stream, String filepath) {
        try {
            boolean exists = minioClient.bucketExists(BucketExistsArgs.builder().bucket(minioConfig.getBucketName()).build());
            if (!exists) {
                minioClient.makeBucket(MakeBucketArgs.builder().bucket(minioConfig.getBucketName()).build());
            }
            PutObjectArgs putObjectArgs = PutObjectArgs.builder()
                    .bucket(minioConfig.getBucketName())
                    .object(filepath.substring(1))
                    .contentType(MediaType.APPLICATION_OCTET_STREAM_VALUE)
                    .stream(stream, stream.available(), -1).build();
            minioClient.putObject(putObjectArgs);
        } catch (Exception e) {
            throw new CustomException("上传文件失败，" + e.getMessage());
        }
        return minioConfig.getEndpoint() + "/" + minioConfig.getBucketName() + filepath;
    }

}
