package com.bluesky.common.result;

import java.io.Serializable;

/**
 * 返回码
 *
 * @author Kevin
 */
public interface IResultCode extends Serializable {

    /**
     * 消息
     *
     * @return String
     */
    String getMessage();

    /**
     * 状态码
     *
     * @return int
     */
    int getCode();

}
