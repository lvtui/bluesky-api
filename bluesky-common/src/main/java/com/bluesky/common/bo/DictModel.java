package com.bluesky.common.bo;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 字典模型
 *
 * @author Kevin
 */
@Data
@Accessors(chain = true)
public class DictModel {

    String label;
    String value;
}
