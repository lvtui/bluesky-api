package com.bluesky.common.enums;

/**
 * 数据源
 *
 * @author Kevin
 */
public enum DataSourceTypeEnum {

    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE
}
