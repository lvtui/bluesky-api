package com.bluesky.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;


/**
 * 性别
 *
 * @author Kevin
 */
@Getter
@AllArgsConstructor
public enum SexEnum {

    /**
     * 男
     */
    MAN("1", "男"),
    /**
     * 女
     */
    WOMAN("2", "女"),
    /**
     * 未知
     */
    UNKNOW("3", "未知");

    private String code;
    private String desc;

}
