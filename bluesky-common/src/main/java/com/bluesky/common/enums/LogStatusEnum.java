package com.bluesky.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;


/**
 * 日志状态
 *
 * @author Kevin
 */
@Getter
@AllArgsConstructor
public enum LogStatusEnum {

    /**
     * 成功
     */
    SUCCESS("0", "成功"),
    /**
     * 删除
     */
    FAILURE("1", "失败");

    private String code;
    private String desc;

}
