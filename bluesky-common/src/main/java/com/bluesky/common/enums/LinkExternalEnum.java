package com.bluesky.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;


/**
 * 是否外链
 *
 * @author Kevin
 */
@Getter
@AllArgsConstructor
public enum LinkExternalEnum {

    /**
     * 是
     */
    YES("0", "是"),
    /**
     * 否
     */
    NO("1", "否");

    private String code;
    private String desc;

}
