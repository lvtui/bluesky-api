package com.bluesky.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;


/**
 * 有效期
 *
 * @author Kevin
 */
@Getter
@AllArgsConstructor
public enum PeriodEnum {
    /**
     * 长期
     */
    LONG_PERIOD("0", "长期"),
    /**
     * 短期
     */
    SHORT_PERIOD("1", "短期");

    private String code;
    private String desc;

}
