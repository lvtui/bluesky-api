package com.bluesky.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 是否缓存
 *
 * @author Kevin
 */
@Getter
@AllArgsConstructor
public enum KeepaliveEnum {

    /**
     * 缓存
     */
    NO("0", "缓存"),
    /**
     * 不缓存
     */
    YES("1", "不缓存");

    private String code;
    private String desc;

}
