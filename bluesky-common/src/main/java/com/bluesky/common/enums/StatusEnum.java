package com.bluesky.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 状态
 *
 * @author Kevin
 */
@Getter
@AllArgsConstructor
public enum StatusEnum {
    /**
     * 正常
     */
    YES("0", "正常"),
    /**
     * 停用
     */
    NO("1", "停用");

    private String code;
    private String desc;

}
