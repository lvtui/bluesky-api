package com.bluesky.common.annotation;

import com.bluesky.common.enums.UserPlatformEnum;

import java.lang.annotation.*;

/**
 * 日志注解
 *
 * @author Kevin
 */
@Target({ElementType.PARAMETER, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Log {
    String title() default "";

    /**
     * 用户平台
     */
    UserPlatformEnum userPlatform() default UserPlatformEnum.WEB;

}
