package com.bluesky.common;

/**
 * 菜单常量类
 *
 * @author Kevin
 */
public class MenuConstants {

    public static final String LAYOUT = "LAYOUT";

    public static final String IFRAME = "IFrame";

    public static final String GET_PARENT_LAYOUT = "getParentLayout('%s')";

}
