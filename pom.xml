<?xml version="1.0" encoding="UTF-8"?>
<project xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns="http://maven.apache.org/POM/4.0.0"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.bluesky</groupId>
    <artifactId>bluesky-api</artifactId>
    <name>${project.artifactId}</name>
    <packaging>pom</packaging>
    <version>1.0</version>

    <properties>
        <java.version>1.8</java.version>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        <versions-maven-plugin.version>2.7</versions-maven-plugin.version>
        <skipTests>true</skipTests>
        <bluesky.project.version>1.0</bluesky.project.version>
        <hutool.version>5.7.16</hutool.version>
        <guava.version>30.1.1-jre</guava.version>
        <druid.version>1.2.5</druid.version>
        <spring-boot.version>2.5.4</spring-boot.version>
        <mybatis-plus.version>3.4.3.3</mybatis-plus.version>
        <mybatis-plus-generator.version>3.4.1</mybatis-plus-generator.version>
        <mysql-connector.version>8.0.13</mysql-connector.version>
        <fastjson.version>1.2.78</fastjson.version>
        <easyexcel.version>2.2.11</easyexcel.version>
        <minio.version>8.3.3</minio.version>
        <okhttp.version>4.9.2</okhttp.version>
        <velocity.version>1.7</velocity.version>
    </properties>

    <modules>
        <module>bluesky-codegen</module>
        <module>bluesky-admin</module>
        <module>bluesky-framework</module>
        <module>bluesky-common</module>
        <module>bluesky-system</module>
        <module>bluesky-oss</module>
    </modules>

    <dependencies>
        <!-- test -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>
        <!-- lombok -->
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
        </dependency>
    </dependencies>

    <!-- 依赖管理 -->
    <dependencyManagement>
        <dependencies>
            <!-- spring boot -->
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-dependencies</artifactId>
                <version>${spring-boot.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
            <!--阿里数据库连接池 -->
            <dependency>
                <groupId>com.alibaba</groupId>
                <artifactId>druid-spring-boot-starter</artifactId>
                <version>${druid.version}</version>
            </dependency>
            <!-- web 模块 -->
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter-web</artifactId>
                <version>${spring-boot.version}</version>
            </dependency>
            <!-- mybatis plus -->
            <dependency>
                <groupId>com.baomidou</groupId>
                <artifactId>mybatis-plus-boot-starter</artifactId>
                <version>${mybatis-plus.version}</version>
            </dependency>
            <!-- mybatis plus generator -->
            <dependency>
                <groupId>com.baomidou</groupId>
                <artifactId>mybatis-plus-generator</artifactId>
                <version>${mybatis-plus-generator.version}</version>
            </dependency>
            <!-- MySQL -->
            <dependency>
                <groupId>mysql</groupId>
                <artifactId>mysql-connector-java</artifactId>
                <version>${mysql-connector.version}</version>
            </dependency>
            <!-- hutool -->
            <dependency>
                <groupId>cn.hutool</groupId>
                <artifactId>hutool-all</artifactId>
                <version>${hutool.version}</version>
            </dependency>
            <!-- guava -->
            <dependency>
                <groupId>com.google.guava</groupId>
                <artifactId>guava</artifactId>
                <version>${guava.version}</version>
            </dependency>
            <!-- fastjson -->
            <dependency>
                <groupId>com.alibaba</groupId>
                <artifactId>fastjson</artifactId>
                <version>${fastjson.version}</version>
            </dependency>
            <!-- easyexcel -->
            <dependency>
                <groupId>com.alibaba</groupId>
                <artifactId>easyexcel</artifactId>
                <version>${easyexcel.version}</version>
            </dependency>
            <!-- minio -->
            <dependency>
                <groupId>io.minio</groupId>
                <artifactId>minio</artifactId>
                <version>${minio.version}</version>
            </dependency>
            <!-- okhttp -->
            <dependency>
                <groupId>com.squareup.okhttp3</groupId>
                <artifactId>okhttp</artifactId>
                <version>${okhttp.version}</version>
            </dependency>
            <!-- 核心模块 -->
            <dependency>
                <groupId>com.bluesky</groupId>
                <artifactId>bluesky-framework</artifactId>
                <version>${bluesky.project.version}</version>
            </dependency>
            <!-- 通用模块 -->
            <dependency>
                <groupId>com.bluesky</groupId>
                <artifactId>bluesky-common</artifactId>
                <version>${bluesky.project.version}</version>
            </dependency>
            <!-- 系统模块 -->
            <dependency>
                <groupId>com.bluesky</groupId>
                <artifactId>bluesky-system</artifactId>
                <version>${bluesky.project.version}</version>
            </dependency>
            <!-- OSS文件存储模块 -->
            <dependency>
                <groupId>com.bluesky</groupId>
                <artifactId>bluesky-oss</artifactId>
                <version>${bluesky.project.version}</version>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <build>
        <finalName>${project.name}</finalName>
        <resources>
            <resource>
                <directory>src/main/resources</directory>
                <filtering>true</filtering>
            </resource>
        </resources>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.springframework.boot</groupId>
                    <artifactId>spring-boot-maven-plugin</artifactId>
                    <version>${spring-boot.version}</version>
                    <configuration>
                        <finalName>${project.build.finalName}</finalName>
                        <layers>
                            <enabled>true</enabled>
                        </layers>
                    </configuration>
                    <executions>
                        <execution>
                            <goals>
                                <goal>repackage</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>

    <profiles>
        <profile>
            <id>dev</id>
            <properties>
                <!-- 环境标识，需要与配置文件的名称相对应 -->
                <profiles.active>dev</profiles.active>
                <logging.level>debug</logging.level>
            </properties>
            <activation>
                <!-- 默认环境 -->
                <activeByDefault>true</activeByDefault>
            </activation>
        </profile>
        <profile>
            <id>test</id>
            <properties>
                <profiles.active>test</profiles.active>
                <logging.level>warn</logging.level>
            </properties>
        </profile>
        <profile>
            <id>prod</id>
            <properties>
                <profiles.active>prod</profiles.active>
                <logging.level>warn</logging.level>
            </properties>
        </profile>
    </profiles>

</project>