package com.bluesky.oss.strategy.impl;

import com.bluesky.common.enums.FileStorageStrategyEnum;
import com.bluesky.common.util.MinioUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * MinIO文件存储策略
 *
 * @author Kevin
 */
@Service
@Slf4j
public class MinioStorageStrategy extends AbstractFileStorage {

    @Resource
    MinioUtils minioUtils;

    @Override
    public String getStrategyName() {
        return FileStorageStrategyEnum.MINIO.name();
    }


    @Override
    public String upload(MultipartFile file, String bizPath) throws IOException {
        // 校验文件
        this.checkFile(file);
        String filepath = this.getFilepath(file, bizPath);
        log.info("上传文件原始名称：{}", file.getOriginalFilename());
        log.info("上传文件路径：{}", filepath);
        String url = minioUtils.uploadObject(file.getInputStream(), filepath);
        log.info("上传文件URL：{}", url);
        return url;
    }
}
