package com.bluesky.oss.strategy;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * 文件存储接口
 *
 * @author Kevin
 */
public interface FileStorageStrategy {

    /**
     * 获取策略名称
     *
     * @return
     */
    String getStrategyName();

    /**
     * 校验文件
     *
     * @param file
     */
    void checkFile(MultipartFile file);

    /**
     * 获取文件上传短路经
     *
     * @param file
     * @param bizPath
     * @return
     * @throws IOException
     */
    String getFilepath(MultipartFile file, String bizPath) throws IOException;

    /**
     * 上传文件
     *
     * @param file
     * @param bizPath
     * @return
     * @throws IOException
     */
    String upload(MultipartFile file, String bizPath) throws IOException;

}
