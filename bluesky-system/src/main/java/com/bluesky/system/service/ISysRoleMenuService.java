package com.bluesky.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bluesky.system.entity.SysRoleMenu;

/**
 * <p>
 * 角色菜单表 服务类
 * </p>
 *
 * @author Kevin
 * @since 2021-07-05
 */
public interface ISysRoleMenuService extends IService<SysRoleMenu> {

}
