package com.bluesky.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bluesky.system.common.dto.SysPostAddDTO;
import com.bluesky.system.common.dto.SysPostEditDTO;
import com.bluesky.system.common.dto.SysPostQueryDTO;
import com.bluesky.system.entity.SysPost;

import java.util.List;

/**
 * <p>
 * 岗位表 服务类
 * </p>
 *
 * @author Kevin
 * @since 2021-06-22
 */
public interface ISysPostService extends IService<SysPost> {

    /**
     * 分页查询
     *
     * @param reqPage
     * @param req
     * @return
     */
    IPage<SysPost> page(Page reqPage, SysPostQueryDTO req);

    /**
     * 查询列表
     *
     * @param req
     * @return
     */
    List<SysPost> list(SysPostQueryDTO req);

    /**
     * 新增
     *
     * @param req
     */
    void add(SysPostAddDTO req);

    /**
     * 修改
     *
     * @param req
     */
    void edit(SysPostEditDTO req);

    /**
     * 删除
     *
     * @param ids
     */
    void remove(String ids);

    /**
     * 查看
     *
     * @param id
     * @return
     */
    SysPost view(String id);

    /**
     * 查询下拉框列表
     *
     * @return
     */
    List<SysPost> listOptions();

    /**
     * 下拉列表
     *
     * @return
     */
    List<SysPost> selectOptions(Long tenantId);

}
