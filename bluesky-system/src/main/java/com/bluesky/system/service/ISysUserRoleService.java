package com.bluesky.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bluesky.system.entity.SysUserRole;

/**
 * <p>
 * 用户角色表 服务类
 * </p>
 *
 * @author Kevin
 * @since 2021-06-10
 */
public interface ISysUserRoleService extends IService<SysUserRole> {

}
