package com.bluesky.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bluesky.common.bo.DictModel;
import com.bluesky.system.common.dto.SysDictAddDTO;
import com.bluesky.system.common.dto.SysDictEditDTO;
import com.bluesky.system.common.dto.SysDictQueryDTO;
import com.bluesky.system.entity.SysDict;

import java.util.List;

/**
 * <p>
 * 字典表 服务类
 * </p>
 *
 * @author Kevin
 * @since 2021-06-10
 */
public interface ISysDictService extends IService<SysDict> {

    /**
     * 分页查询
     *
     * @param reqPage
     * @param req
     * @return
     */
    IPage<SysDict> page(Page reqPage, SysDictQueryDTO req);

    /**
     * 查询列表
     *
     * @param req
     * @return
     */
    List<SysDict> list(SysDictQueryDTO req);

    /**
     * 新增
     *
     * @param req
     */
    void add(SysDictAddDTO req);

    /**
     * 修改
     *
     * @param req
     */
    void edit(SysDictEditDTO req);

    /**
     * 删除
     *
     * @param ids
     */
    void remove(String ids);

    /**
     * 查看
     *
     * @param id
     * @return
     */
    SysDict view(String id);

    /**
     * 根据字典编码，查询字典项列表
     *
     * @param dictCode
     * @return
     */
    List<DictModel> listDictModel(String dictCode);

    /**
     * 加载数据字典缓存
     */
    void loadAllDictCache();

}
