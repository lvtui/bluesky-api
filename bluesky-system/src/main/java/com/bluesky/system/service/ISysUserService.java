package com.bluesky.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bluesky.system.common.dto.SysUserAddDTO;
import com.bluesky.system.common.dto.SysUserEditDTO;
import com.bluesky.system.common.dto.SysUserQueryDTO;
import com.bluesky.system.common.dto.SysUserResetPwdDTO;
import com.bluesky.system.entity.SysUser;

import java.util.List;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author Kevin
 * @since 2021-06-10
 */
public interface ISysUserService extends IService<SysUser> {

    /**
     * 分页查询
     *
     * @param reqPage
     * @param req
     * @return
     */
    IPage<SysUser> page(Page reqPage, SysUserQueryDTO req);

    /**
     * 查询列表
     *
     * @param req
     * @return
     */
    List<SysUser> list(SysUserQueryDTO req);

    /**
     * 新增
     *
     * @param req
     */
    void add(SysUserAddDTO req);

    /**
     * 修改
     *
     * @param req
     */
    void edit(SysUserEditDTO req);

    /**
     * 删除
     *
     * @param ids
     */
    void remove(String ids);

    /**
     * 查看
     *
     * @param id
     * @return
     */
    SysUser view(String id);

    /**
     * 重置密码
     *
     * @param req
     */
    void resetPwd(SysUserResetPwdDTO req);

}
