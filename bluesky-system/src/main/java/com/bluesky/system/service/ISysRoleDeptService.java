package com.bluesky.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bluesky.system.entity.SysRoleDept;

/**
 * <p>
 * 角色部门表 服务类
 * </p>
 *
 * @author Kevin
 * @since 2021-06-10
 */
public interface ISysRoleDeptService extends IService<SysRoleDept> {

}
