package com.bluesky.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bluesky.system.entity.SysUserPost;

/**
 * <p>
 * 用户岗位表 服务类
 * </p>
 *
 * @author Kevin
 * @since 2021-06-10
 */
public interface ISysUserPostService extends IService<SysUserPost> {

}
