package com.bluesky.system.service.impl;

import cn.hutool.core.lang.tree.Tree;
import cn.hutool.core.lang.tree.TreeNode;
import cn.hutool.core.lang.tree.TreeUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bluesky.system.common.dto.SysAreaQueryDTO;
import com.bluesky.system.entity.SysArea;
import com.bluesky.system.mapper.SysAreaMapper;
import com.bluesky.system.service.ISysAreaService;
import com.google.common.collect.Lists;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * <p>
 * 行政区域 服务实现类
 * </p>
 *
 * @author Kevin
 * @since 2021-08-08
 */
@Service
public class SysAreaServiceImpl extends ServiceImpl<SysAreaMapper, SysArea> implements ISysAreaService {

    @Override
    public IPage<SysArea> page(Page reqPage, SysAreaQueryDTO req) {
        LambdaQueryWrapper<SysArea> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(Objects.nonNull(req.getParentCode()), SysArea::getParentCode, req.getParentCode());
        queryWrapper.like(StrUtil.isNotBlank(req.getAreaCode()), SysArea::getAreaCode, req.getAreaCode());
        queryWrapper.like(StrUtil.isNotBlank(req.getAreaName()), SysArea::getAreaName, req.getAreaName());
        queryWrapper.orderByAsc(SysArea::getSort);
        IPage<SysArea> page = this.page(reqPage, queryWrapper);
        List<SysArea> sysAreaList = this.list();
        page.getRecords().forEach(item -> {
            item.setChildren(getChildrenList(sysAreaList, item));
            if (item.getParentCode().equals("0")) {
                item.setParentCode(null);
            }
        });
        return page;
    }

    @Override
    public List<Tree<String>> listAreaTree() {
        List<SysArea> sysAreaList = this.list();
        if (sysAreaList.isEmpty()) {
            return Lists.newArrayList();
        }
        List treeNodeList = sysAreaList.stream().map(item -> {
            TreeNode treeNode = new TreeNode();
            treeNode.setId(item.getAreaCode());
            treeNode.setName(item.getAreaName());
            treeNode.setParentId(item.getParentCode());
            treeNode.setWeight(item.getSort());
            return treeNode;
        }).collect(Collectors.toList());
        List<Tree<String>> treeList = TreeUtil.build(treeNodeList, "0");
        return treeList;
    }

    private List<SysArea> getChildrenList(List<SysArea> sysAreaList, SysArea root) {
        List<SysArea> childrenList = sysAreaList.stream()
                .filter(item -> item.getParentCode().equals(root.getAreaCode()))
                .sorted(Comparator.comparing(SysArea::getSort))
                .collect(Collectors.toList());
        childrenList.forEach(item -> {
            item.setChildren(getChildrenList(sysAreaList, item));
        });
        return childrenList;
    }

}
