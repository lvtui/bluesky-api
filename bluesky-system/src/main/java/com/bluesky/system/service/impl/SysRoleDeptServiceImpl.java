package com.bluesky.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bluesky.system.entity.SysRoleDept;
import com.bluesky.system.mapper.SysRoleDeptMapper;
import com.bluesky.system.service.ISysRoleDeptService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色部门表 服务实现类
 * </p>
 *
 * @author Kevin
 * @since 2021-06-10
 */
@Service
public class SysRoleDeptServiceImpl extends ServiceImpl<SysRoleDeptMapper, SysRoleDept> implements ISysRoleDeptService {


}
