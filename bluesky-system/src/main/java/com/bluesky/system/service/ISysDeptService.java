package com.bluesky.system.service;

import cn.hutool.core.lang.tree.Tree;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bluesky.system.common.dto.SysDeptAddDTO;
import com.bluesky.system.common.dto.SysDeptEditDTO;
import com.bluesky.system.common.dto.SysDeptQueryDTO;
import com.bluesky.system.entity.SysDept;

import java.util.List;

/**
 * <p>
 * 部门表 服务类
 * </p>
 *
 * @author Kevin
 * @since 2021-06-10
 */
public interface ISysDeptService extends IService<SysDept> {

    /**
     * 分页查询
     *
     * @param reqPage
     * @param req
     * @return
     */
    IPage<SysDept> page(Page reqPage, SysDeptQueryDTO req);

    /**
     * 新增
     *
     * @param req
     */
    void add(SysDeptAddDTO req);

    /**
     * 修改
     *
     * @param req
     */
    void edit(SysDeptEditDTO req);

    /**
     * 删除
     *
     * @param ids
     */
    void remove(String ids);

    /**
     * 查看
     *
     * @param id
     * @return
     */
    SysDept view(String id);

    /**
     * 查询部门树
     *
     * @return
     */
    List<Tree<String>> listDeptTree();

    /**
     * 查询部门树，排除当前节点以及子节点
     *
     * @param nodeId
     * @return
     */
    List<Tree<String>> listDeptTreeAndExcludeNode(String nodeId);

    /**
     * 下拉树，排除当前节点以及子节点
     *
     * @param nodeId
     * @return
     */
    List<Tree<String>> selectTreeAndExcludeNode(Long tenantId, String nodeId);

    /**
     * 下拉部门树
     *
     * @return
     */
    List<Tree<String>> selectTree(Long tenantId);

}
