package com.bluesky.system.entity;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.*;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.bluesky.common.annotation.ExcelDict;
import com.bluesky.common.convert.ExcelDictConverter;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.apache.poi.ss.usermodel.HorizontalAlignment;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author Kevin
 * @since 2021-06-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ColumnWidth(15)
@HeadRowHeight(15)
@ContentRowHeight(15)
@HeadStyle
@HeadFontStyle(fontHeightInPoints = 12)
@ContentStyle(horizontalAlignment = HorizontalAlignment.CENTER)
@ContentFontStyle(fontHeightInPoints = 10)
@ExcelIgnoreUnannotated
public class SysUser implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 租户ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long tenantId;

    /**
     * 账号
     */
    @ExcelProperty(value = "账号", index = 0)
    private String account;

    /**
     * 密码
     */

    @JsonIgnore
    private String password;

    /**
     * 修改密码标记 0未修改；1已修改
     */

    @JsonIgnore
    private String pswModified;

    /**
     * 昵称
     */
    @ExcelProperty(value = "昵称", index = 2)
    private String nickname;

    /**
     * 姓名
     */
    @ExcelProperty(value = "姓名", index = 4)
    private String realname;

    /**
     * 英文名
     */
    @ExcelProperty(value = "英文名", index = 5)
    private String englishName;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 邮箱
     */
    @ExcelProperty(value = "邮箱", index = 6)
    private String email;

    /**
     * 手机号
     */
    @ExcelProperty(value = "手机号", index = 7)
    private String phone;

    /**
     * 工号
     */
    @ExcelProperty(value = "工号", index = 8)
    private String staffNumber;

    /**
     * 生日
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @ColumnWidth(20)
    @ExcelProperty(value = "生日", index = 9)
    private Date birthday;

    /**
     * 性别 1男；2女；3未知
     */
    @ExcelProperty(value = "性别", index = 3, converter = ExcelDictConverter.class)
    @ExcelDict("sys_sex")
    private String sex;

    /**
     * 部门ID
     */
    private String deptId;

    /**
     * 锁定标记 0正常；1锁定
     */
    @ExcelProperty(value = "锁定", index = 10, converter = ExcelDictConverter.class)
    @ExcelDict("sys_lock_flag")
    private String lockFlag;

    /**
     * 排序
     */
    @ExcelProperty(value = "排序", index = 11)
    private Integer sort;

    /**
     * 备注
     */
    @ExcelProperty(value = "备注", index = 13)
    private String remarks;

    /**
     * 状态 0正常；1停用
     */
    @ExcelProperty(value = "状态", index = 12, converter = ExcelDictConverter.class)
    @ExcelDict("sys_status")
    private String status;

    /**
     * 删除标记 0存在；1删除
     */
    private String delFlag;

    /**
     * 创建人
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 更新人
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateBy;

    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    /**
     * 部门名称
     */
    @TableField(exist = false)
    @ExcelProperty(value = "部门", index = 1)
    private String deptName;

    /**
     * 岗位组
     */
    @TableField(exist = false)
    private List<String> postIds;

    /**
     * 角色组
     */
    @TableField(exist = false)
    private List<String> roleIds;

    /**
     * 租户名称
     */
    @TableField(exist = false)
    private String tenantName;

}