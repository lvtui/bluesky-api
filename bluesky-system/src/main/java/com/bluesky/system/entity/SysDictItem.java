package com.bluesky.system.entity;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.*;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.bluesky.common.annotation.ExcelDict;
import com.bluesky.common.convert.ExcelDictConverter;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.apache.poi.ss.usermodel.HorizontalAlignment;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 字典项
 * </p>
 *
 * @author Kevin
 * @since 2021-06-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ColumnWidth(15)
@HeadRowHeight(15)
@ContentRowHeight(15)
@HeadStyle
@HeadFontStyle(fontHeightInPoints = 12)
@ContentStyle(horizontalAlignment = HorizontalAlignment.CENTER)
@ContentFontStyle(fontHeightInPoints = 10)
@ExcelIgnoreUnannotated
public class SysDictItem implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 字典ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long dictId;

    /**
     * 字典项编码
     */
    @ExcelProperty(value = "字典项编码", index = 1)
    private String dictItemCode;

    /**
     * 字典项名称
     */
    @ExcelProperty(value = "字典项名称", index = 0)
    private String dictItemName;

    /**
     * 排序
     */
    @ExcelProperty(value = "排序", index = 2)
    private Integer sort;

    /**
     * 备注
     */
    @ExcelProperty(value = "备注", index = 4)
    private String remarks;

    /**
     * 状态 0正常；1停用
     */
    @ExcelProperty(value = "状态", index = 3, converter = ExcelDictConverter.class)
    @ExcelDict("sys_status")
    private String status;

    /**
     * 创建人
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 更新人
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateBy;

    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;


}