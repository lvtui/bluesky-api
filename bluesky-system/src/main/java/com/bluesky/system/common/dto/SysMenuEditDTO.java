package com.bluesky.system.common.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * <p>
 * 菜单表
 * </p>
 *
 * @author Kevin
 * @since 2021-06-19
 */
@Data
@Accessors(chain = true)
public class SysMenuEditDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @NotNull(message = "主键不能为空")
    private Long id;

    /**
     * 菜单类型 dir目录；menu菜单；button按钮
     */
    @NotBlank(message = "菜单类型")
    @Size(max = 30, message = "菜单类型长度不能超过30个字符")
    private String menuType;

    /**
     * 菜单名称
     */
    @NotBlank(message = "菜单名称不能为空")
    @Size(max = 100, message = "菜单名称长度不能超过100个字符")
    private String menuName;

    /**
     * 上级菜单
     */
    private Long parentId;

    /**
     * 路由地址
     */
    @Size(max = 500, message = "路由地址长度不能超过500个字符")
    private String routePath;

    /**
     * 组件路径
     */
    @Size(max = 500, message = "组件路径长度不能超过500个字符")
    private String component;

    /**
     * 权限标识
     */
    @Size(max = 100, message = "权限标识长度不能超过100个字符")
    private String permission;

    /**
     * 图标
     */
    @Size(max = 100, message = "图标长度不能超过100个字符")
    private String icon;

    /**
     * 是否缓存 0缓存；1不缓存
     */
    private String keepalive;

    /**
     * 是否外链 0是；1否
     */
    private String linkExternal;

    /**
     * 是否显示 0显示；1隐藏
     */
    private String visible;

    /**
     * 是否内嵌 0内嵌；1不内嵌
     */
    private String frame;

    /**
     * 外部链接
     */
    @Size(max = 500, message = "外部链接长度不能超过500个字符")
    private String linkUrl;

    /**
     * 排序
     */
    @NotNull(message = "排序不能为空")
    private Integer sort;

    /**
     * 备注
     */
    @Size(max = 500, message = "备注长度不能超过500个字符")
    private String remarks;

    /**
     * 状态 0正常；1停用
     */
    @NotBlank(message = "状态不能为空")
    private String status;

}