package com.bluesky.system.common.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 部门表
 * </p>
 *
 * @author Kevin
 * @since 2021-06-10
 */
@Data
@Accessors(chain = true)
public class SysDeptQueryDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 父部门ID
     */
    private Long parentId;

    /**
     * 部门名称
     */
    private String deptName;

    /**
     * 部门全名
     */
    private String deptFullname;

    /**
     * 祖级列表
     */
    private String ancestors;

    /**
     * 机构类型 1公司；2部门；3小组；4其他
     */
    private String orgType;

    /**
     * 负责人
     */
    private String leader;

    /**
     * 负责人电话
     */
    private String leaderPhone;

    /**
     * 办公电话
     */
    private String phone;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 邮政编码
     */
    private String postCode;

    /**
     * 联系地址
     */
    private String address;

}