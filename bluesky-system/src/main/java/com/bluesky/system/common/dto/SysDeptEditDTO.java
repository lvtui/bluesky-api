package com.bluesky.system.common.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * 部门表
 * </p>
 *
 * @author Kevin
 * @since 2021-06-10
 */
@Data
@Accessors(chain = true)
public class SysDeptEditDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @NotNull(message = "主键不能为空")
    private Long id;

    /**
     * 租户ID
     */
    private Long tenantId;

    /**
     * 父部门ID
     */
    private Long parentId;

    /**
     * 部门名称
     */
    @NotBlank(message = "部门名称不能为空")
    private String deptName;

    /**
     * 部门全名
     */
    @NotBlank(message = "部门全名不能为空")
    private String deptFullname;

    /**
     * 祖级列表
     */
    private String ancestors;

    /**
     * 机构类型 1公司；2部门；3小组；4其他
     */
    @NotBlank(message = "机构类型 1公司；2部门；3小组；4其他不能为空")
    private String orgType;

    /**
     * 负责人
     */
    private String leader;

    /**
     * 负责人电话
     */
    private String leaderPhone;

    /**
     * 办公电话
     */
    private String phone;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 邮政编码
     */
    private String postCode;

    /**
     * 联系地址
     */
    private String address;

    /**
     * 排序
     */
    @NotNull(message = "排序不能为空")
    private Integer sort;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 状态 0正常；1停用
     */
    @NotBlank(message = "状态 0正常；1停用不能为空")
    private String status;


}