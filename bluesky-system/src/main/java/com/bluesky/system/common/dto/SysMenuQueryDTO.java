package com.bluesky.system.common.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 菜单表
 * </p>
 *
 * @author Kevin
 * @since 2021-06-19
 */
@Data
@Accessors(chain = true)
public class SysMenuQueryDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 菜单名称
     */
    private String menuName;

    /**
     * 上级菜单
     */
    private Long parentId;

}