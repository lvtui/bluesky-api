package com.bluesky.system.common.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * <p>
 * 字典项
 * </p>
 *
 * @author Kevin
 * @since 2021-06-10
 */
@Data
@Accessors(chain = true)
public class SysDictItemEditDTO implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * 主键
     */
    @NotNull(message = "主键不能为空")
    private Long id;

    /**
     * 字典ID
     */
    @NotNull(message = "字典ID不能为空")
    private Long dictId;

    /**
     * 字典项编码
     */
    @NotBlank(message = "字典项编码不能为空")
    @Size(max = 100, message = "字典项编码长度不能超过100个字符")
    private String dictItemCode;

    /**
     * 字典项名称
     */
    @NotBlank(message = "字典项名称不能为空")
    @Size(max = 100, message = "字典项名称长度不能超过100个字符")
    private String dictItemName;

    /**
     * 排序
     */
    @NotNull(message = "排序不能为空")
    private Integer sort;

    /**
     * 备注
     */
    @Size(max = 500, message = "备注长度不能超过500个字符")
    private String remarks;

    /**
     * 状态 0正常；1停用
     */
    @NotBlank(message = "状态不能为空")
    private String status;


}