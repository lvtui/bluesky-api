package com.bluesky.system.common.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 角色表
 * </p>
 *
 * @author Kevin
 * @since 2021-06-10
 */
@Data
@Accessors(chain = true)
public class SysRoleAssignMenuDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 角色ID
     */
    @NotNull(message = "角色ID不能为空")
    private Long roleId;

    /**
     * 菜单组
     */
    private List<Long> menuIds;

}