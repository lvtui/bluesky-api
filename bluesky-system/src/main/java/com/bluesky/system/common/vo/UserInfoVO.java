package com.bluesky.system.common.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

/**
 * 用户信息VO
 *
 * @author Kevin
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class UserInfoVO implements Serializable {

    private String userId;

    private String username;

    private String realName;

    private String avatar;

    private String desc;


    private List<RoleInfoVO> roles;

    @JsonSerialize(using = ToStringSerializer.class)
    private Long tenantId;

    private String tenantCode;

    private String tenantTitle;

    private String tenantLogo;

}
