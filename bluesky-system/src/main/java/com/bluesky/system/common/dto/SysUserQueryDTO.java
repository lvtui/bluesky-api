package com.bluesky.system.common.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author Kevin
 * @since 2021-06-10
 */
@Data
@Accessors(chain = true)
public class SysUserQueryDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 账号
     */
    private String account;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 姓名
     */
    private String realname;

    /**
     * 英文名
     */
    private String englishName;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 工号
     */
    private String staffNumber;

    /**
     * 部门ID
     */
    private String deptId;
}