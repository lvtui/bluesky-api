package com.bluesky.system.common.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 角色表
 * </p>
 *
 * @author Kevin
 * @since 2021-06-10
 */
@Data
@Accessors(chain = true)
public class SysRoleAddDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 租户ID
     */
    private Long tenantId;

    /**
     * 角色编码
     */
    @NotBlank(message = "角色编码不能为空")
    @Size(max = 100, message = "字典编码长度不能超过100个字符")
    private String roleCode;

    /**
     * 角色名称
     */
    @NotBlank(message = "角色名称不能为空")
    @Size(max = 100, message = "字典编码长度不能超过100个字符")
    private String roleName;

    /**
     * 数据范围 1全部数据权限；2自定数据权限；3本部门数据权限；4本部门及以下数据权限
     */
    @NotBlank(message = "数据范围不能为空")
    private String dataScope;

    /**
     * 备注
     */
    @Size(max = 500, message = "备注长度不能超过500个字符")
    private String remarks;

    /**
     * 排序
     */
    @NotNull(message = "排序不能为空")
    private Integer sort;

    /**
     * 状态 0正常；1停用
     */
    @NotBlank(message = "状态不能为空")
    private String status;

    /**
     * 部门ID列表
     */
    private List<Long> deptIds;

}