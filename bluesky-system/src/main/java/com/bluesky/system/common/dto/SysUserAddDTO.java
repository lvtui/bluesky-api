package com.bluesky.system.common.dto;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author Kevin
 * @since 2021-06-10
 */
@Data
@Accessors(chain = true)
public class SysUserAddDTO implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * 主键
     */
    private Long id;

    /**
     * 租户ID
     */
    private Long tenantId;

    /**
     * 账号
     */
    @NotBlank(message = "账号不能为空")
    @Size(max = 100, message = "账号长度不能超过100个字符")
    private String account;

    /**
     * 密码
     */
    @NotBlank(message = "密码不能为空")
    @Size(max = 100, message = "密码长度不能超过100个字符")
    private String password;

    /**
     * 昵称
     */
    @NotBlank(message = "昵称不能为空")
    @Size(max = 100, message = "昵称长度不能超过100个字符")
    private String nickname;

    /**
     * 姓名
     */
    @Size(max = 100, message = "姓名长度不能超过100个字符")
    private String realname;

    /**
     * 英文名
     */
    @Size(max = 100, message = "英文名长度不能超过100个字符")
    private String englishName;

    /**
     * 头像
     */
    @Size(max = 100, message = "头像长度不能超过100个字符")
    private String avatar;

    /**
     * 邮箱
     */
    @Size(max = 100, message = "邮箱长度不能超过100个字符")
    private String email;

    /**
     * 手机号
     */
    @Size(max = 30, message = "手机号长度不能超过30个字符")
    private String phone;

    /**
     * 工号
     */
    @Size(max = 30, message = "工号长度不能超过30个字符")
    private String staffNumber;

    /**
     * 生日
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date birthday;

    /**
     * 性别 1男；2女；3未知
     */
    @NotBlank(message = "性别不能为空")
    private String sex;

    /**
     * 部门ID
     */
    private String deptId;

    /**
     * 锁定标记 0正常；1锁定
     */
    @NotBlank(message = "锁定标记不能为空")
    private String lockFlag;

    /**
     * 排序
     */
    @NotNull(message = "排序不能为空")
    private Integer sort;

    /**
     * 备注
     */
    @Size(max = 500, message = "备注长度不能超过500个字符")
    private String remarks;

    /**
     * 状态 0正常；1停用
     */
    @NotBlank(message = "状态不能为空")
    private String status;

    /**
     * 岗位组
     */
    private List<Long> postIds;

    /**
     * 角色组
     */
    private List<Long> roleIds;

}