package com.bluesky.system.common.dto;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 租户表
 * </p>
 *
 * @author Kevin
 * @since 2021-09-29
 */
@Data
@Accessors(chain = true)
public class SysTenantEditDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @NotNull(message = "主键不能为空")
    private Long id;

    /**
     * 租户编码
     */
    @NotBlank(message = "租户编码不能为空")
    @Size(max = 100, message = "租户编码长度不能超过100个字符")
    private String tenantCode;

    /**
     * 租户名称
     */
    @NotBlank(message = "租户名称不能为空")
    @Size(max = 100, message = "租户名称长度不能超过100个字符")
    private String tenantName;

    /**
     * 网站标题
     */
    @NotBlank(message = "网站标题不能为空")
    @Size(max = 100, message = "网站标题长度不能超过100个字符")
    private String tenantTitle;

    /**
     * 网站LOGO
     */
    @Size(max = 500, message = "网站LOGO长度不能超过500个字符")
    private String tenantLogo;

    /**
     * 租户时效 0长期；1短期
     */
    @NotBlank(message = "租户时效不能为空")
    private String tenantPeriod;

    /**
     * 租户开始时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date startDate;

    /**
     * 租户结束时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endDate;

    /**
     * 备注
     */
    @Size(max = 500, message = "备注长度不能超过500个字符")
    private String remarks;

    /**
     * 状态 0正常；1停用
     */
    @NotBlank(message = "状态不能为空")
    private String status;

}