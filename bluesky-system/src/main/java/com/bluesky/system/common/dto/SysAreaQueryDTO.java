package com.bluesky.system.common.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 行政区域
 * </p>
 *
 * @author Kevin
 * @since 2021-08-08
 */
@Data
@Accessors(chain = true)
public class SysAreaQueryDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 区域编码
     */
    private String areaCode;

    /**
     * 区域名称
     */
    private String areaName;

    /**
     * 父编码
     */
    private String parentCode;

}