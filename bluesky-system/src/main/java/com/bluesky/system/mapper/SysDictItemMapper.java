package com.bluesky.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bluesky.system.entity.SysDictItem;

/**
 * <p>
 * 字典项 Mapper 接口
 * </p>
 *
 * @author Kevin
 * @since 2021-06-10
 */
public interface SysDictItemMapper extends BaseMapper<SysDictItem> {

}
