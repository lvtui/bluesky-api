package com.bluesky.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bluesky.system.entity.SysRoleDept;

/**
 * <p>
 * 角色部门表 Mapper 接口
 * </p>
 *
 * @author Kevin
 * @since 2021-06-10
 */
public interface SysRoleDeptMapper extends BaseMapper<SysRoleDept> {

}
