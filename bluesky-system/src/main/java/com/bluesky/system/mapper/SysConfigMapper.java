package com.bluesky.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bluesky.system.entity.SysConfig;

/**
 * <p>
 * 系统配置 Mapper 接口
 * </p>
 *
 * @author Kevin
 * @since 2021-06-10
 */
public interface SysConfigMapper extends BaseMapper<SysConfig> {

}
