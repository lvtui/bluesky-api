package com.bluesky.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bluesky.system.entity.SysArea;

/**
 * <p>
 * 行政区域 Mapper 接口
 * </p>
 *
 * @author Kevin
 * @since 2021-08-08
 */
public interface SysAreaMapper extends BaseMapper<SysArea> {

}
