package com.bluesky.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bluesky.system.entity.SysLog;

/**
 * <p>
 * 操作日志表  Mapper 接口
 * </p>
 *
 * @author Kevin
 * @since 2021-07-12
 */
public interface SysLogMapper extends BaseMapper<SysLog> {

}
