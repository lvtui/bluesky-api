package com.bluesky.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bluesky.system.entity.SysPost;

/**
 * <p>
 * 岗位表 Mapper 接口
 * </p>
 *
 * @author Kevin
 * @since 2021-06-22
 */
public interface SysPostMapper extends BaseMapper<SysPost> {

}
