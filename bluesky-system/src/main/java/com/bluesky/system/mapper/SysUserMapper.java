package com.bluesky.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bluesky.system.entity.SysUser;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author Kevin
 * @since 2021-06-10
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

}
