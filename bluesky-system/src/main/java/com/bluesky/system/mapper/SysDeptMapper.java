package com.bluesky.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bluesky.system.entity.SysDept;

/**
 * <p>
 * 部门表 Mapper 接口
 * </p>
 *
 * @author Kevin
 * @since 2021-06-10
 */
public interface SysDeptMapper extends BaseMapper<SysDept> {

}
