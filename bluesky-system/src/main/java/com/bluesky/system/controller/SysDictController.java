package com.bluesky.system.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bluesky.common.annotation.Log;
import com.bluesky.common.result.R;
import com.bluesky.common.util.ExcelUtils;
import com.bluesky.system.common.dto.SysDictAddDTO;
import com.bluesky.system.common.dto.SysDictEditDTO;
import com.bluesky.system.common.dto.SysDictQueryDTO;
import com.bluesky.system.entity.SysDict;
import com.bluesky.system.service.ISysDictService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 * 字典表 前端控制器
 * </p>
 *
 * @author Kevin
 * @since 2021-06-10
 */
@RestController
@RequestMapping("/system/sysDict")
public class SysDictController {

    @Resource
    private ISysDictService sysDictService;

    /**
     * 分页查询
     */
    @GetMapping("/page")
    @PreAuthorize("@ss.hasPermission('system:sysDict:page')")
    @Log(title = "字典管理分页查询")
    public R page(Page reqPage, SysDictQueryDTO req) {
        return R.success(sysDictService.page(reqPage, req));
    }

    /**
     * 新增
     */
    @PostMapping("/add")
    @PreAuthorize("@ss.hasPermission('system:sysDict:add')")
    @Log(title = "字典管理新增")
    public R add(@Validated @RequestBody SysDictAddDTO req) {
        sysDictService.add(req);
        return R.success();
    }

    /**
     * 修改
     */
    @PostMapping("/edit")
    @PreAuthorize("@ss.hasPermission('system:sysDict:edit')")
    @Log(title = "字典管理修改")
    public R edit(@Validated @RequestBody SysDictEditDTO req) {
        sysDictService.edit(req);
        return R.success();
    }

    /**
     * 删除
     */
    @PostMapping("/remove")
    @PreAuthorize("@ss.hasPermission('system:sysDict:remove')")
    @Log(title = "字典管理删除")
    public R remove(@RequestParam String ids) {
        sysDictService.remove(ids);
        return R.success();
    }

    /**
     * 查看
     */
    @GetMapping("/view")
    @PreAuthorize("@ss.hasPermission('system:sysDict:view')")
    @Log(title = "字典管理查看")
    public R view(@RequestParam String id) {
        return R.success(sysDictService.view(id));
    }

    /**
     * 导出
     */
    @GetMapping("/export")
    @PreAuthorize("@ss.hasPermission('system:sysDict:export')")
    @Log(title = "字典管理导出")
    public R export(SysDictQueryDTO req) {
        String filepath = ExcelUtils.export("字典列表", SysDict.class, sysDictService.list(req));
        return R.success(filepath);
    }

    /**
     * 根据字典编码，查询字典项列表
     */
    @GetMapping("/listDictModel")
    public R listDictModel(@RequestParam String dictCode) {
        return R.success(sysDictService.listDictModel(dictCode));
    }

    /**
     * 刷新数据字典缓存
     */
    @PostMapping("/refresh")
    @PreAuthorize("@ss.hasPermission('system:sysDict:refresh')")
    @Log(title = "字典管理刷新缓存")
    public R refresh() {
        sysDictService.loadAllDictCache();
        return R.success();
    }

}

