package com.bluesky.system.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bluesky.common.annotation.Log;
import com.bluesky.common.result.R;
import com.bluesky.common.util.ExcelUtils;
import com.bluesky.common.util.SecurityUtils;
import com.bluesky.system.common.dto.SysPostAddDTO;
import com.bluesky.system.common.dto.SysPostEditDTO;
import com.bluesky.system.common.dto.SysPostQueryDTO;
import com.bluesky.system.entity.SysPost;
import com.bluesky.system.service.ISysPostService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * <p>
 * 岗位表 前端控制器
 * </p>
 *
 * @author Kevin
 * @since 2021-06-22
 */
@RestController
@RequestMapping("/system/sysPost")
public class SysPostController {

    @Resource
    private ISysPostService sysPostService;

    /**
     * 分页查询
     */
    @GetMapping("/page")
    @PreAuthorize("@ss.hasPermission('system:sysPost:page')")
    @Log(title = "岗位管理分页查询")
    public R page(Page reqPage, SysPostQueryDTO req) {
        return R.success(sysPostService.page(reqPage, req));
    }

    /**
     * 新增
     */
    @PostMapping("/add")
    @PreAuthorize("@ss.hasPermission('system:sysPost:add')")
    @Log(title = "岗位管理新增")
    public R add(@Validated @RequestBody SysPostAddDTO req) {
        if (Objects.isNull(req.getTenantId())) {
            req.setTenantId(SecurityUtils.getTenantId());
        }
        sysPostService.add(req);
        return R.success();
    }

    /**
     * 修改
     */
    @PostMapping("/edit")
    @PreAuthorize("@ss.hasPermission('system:sysPost:edit')")
    @Log(title = "岗位管理修改")
    public R edit(@Validated @RequestBody SysPostEditDTO req) {
        sysPostService.edit(req);
        return R.success();
    }

    /**
     * 删除
     */
    @PostMapping("/remove")
    @PreAuthorize("@ss.hasPermission('system:sysPost:remove')")
    @Log(title = "岗位管理删除")
    public R remove(@RequestParam String ids) {
        sysPostService.remove(ids);
        return R.success();
    }

    /**
     * 查看
     */
    @GetMapping("/view")
    @PreAuthorize("@ss.hasPermission('system:sysPost:view')")
    @Log(title = "岗位管理查看")
    public R view(@RequestParam String id) {
        return R.success(sysPostService.view(id));
    }

    /**
     * 导出
     */
    @GetMapping("/export")
    @PreAuthorize("@ss.hasPermission('system:sysPost:export')")
    @Log(title = "岗位管理导出")
    public R export(SysPostQueryDTO req) {
        String filepath = ExcelUtils.export("岗位列表", SysPost.class, sysPostService.list(req));
        return R.success(filepath);
    }

    /**
     * 查询下拉框列表
     */
    @GetMapping("/listOptions")
    public R listOptions() {
        return R.success(sysPostService.listOptions());
    }

    /**
     * 下拉列表
     */
    @GetMapping("/selectOptions")
    public R selectOptions(@RequestParam(required = false) Long tenantId) {
        tenantId = Objects.isNull(tenantId) ? SecurityUtils.getTenantId() : tenantId;
        return R.success(sysPostService.selectOptions(tenantId));
    }

}

