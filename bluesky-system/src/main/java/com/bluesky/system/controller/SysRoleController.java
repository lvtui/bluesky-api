package com.bluesky.system.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bluesky.common.annotation.Log;
import com.bluesky.common.result.R;
import com.bluesky.common.util.ExcelUtils;
import com.bluesky.common.util.SecurityUtils;
import com.bluesky.system.common.dto.SysRoleAddDTO;
import com.bluesky.system.common.dto.SysRoleAssignMenuDTO;
import com.bluesky.system.common.dto.SysRoleEditDTO;
import com.bluesky.system.common.dto.SysRoleQueryDTO;
import com.bluesky.system.entity.SysRole;
import com.bluesky.system.service.ISysRoleService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * <p>
 * 角色表 前端控制器
 * </p>
 *
 * @author Kevin
 * @since 2021-06-10
 */
@RestController
@RequestMapping("/system/sysRole")
public class SysRoleController {

    @Resource
    private ISysRoleService sysRoleService;

    /**
     * 分页查询
     */
    @GetMapping("/page")
    @PreAuthorize("@ss.hasPermission('system:sysRole:page')")
    @Log(title = "角色管理分页查询")
    public R page(Page reqPage, SysRoleQueryDTO req) {
        return R.success(sysRoleService.page(reqPage, req));
    }

    /**
     * 新增
     */
    @PostMapping("/add")
    @PreAuthorize("@ss.hasPermission('system:sysRole:add')")
    @Log(title = "角色管理新增")
    public R add(@Validated @RequestBody SysRoleAddDTO req) {
        if (Objects.isNull(req.getTenantId())) {
            req.setTenantId(SecurityUtils.getTenantId());
        }
        sysRoleService.add(req);
        return R.success();
    }

    /**
     * 修改
     */
    @PostMapping("/edit")
    @PreAuthorize("@ss.hasPermission('system:sysRole:edit')")
    @Log(title = "角色管理修改")
    public R edit(@Validated @RequestBody SysRoleEditDTO req) {
        sysRoleService.edit(req);
        return R.success();
    }

    /**
     * 删除
     */
    @PostMapping("/remove")
    @PreAuthorize("@ss.hasPermission('system:sysRole:remove')")
    @Log(title = "角色管理删除")
    public R remove(@RequestParam String ids) {
        sysRoleService.remove(ids);
        return R.success();
    }

    /**
     * 查看
     */
    @GetMapping("/view")
    @PreAuthorize("@ss.hasPermission('system:sysRole:view')")
    @Log(title = "角色管理查看")
    public R view(@RequestParam String id) {
        return R.success(sysRoleService.view(id));
    }

    /**
     * 导出
     */
    @GetMapping("/export")
    @PreAuthorize("@ss.hasPermission('system:sysRole:export')")
    @Log(title = "角色管理导出")
    public R export(SysRoleQueryDTO req) {
        String filepath = ExcelUtils.export("角色列表", SysRole.class, sysRoleService.list(req));
        return R.success(filepath);
    }

    /**
     * 查询下拉框列表
     */
    @GetMapping("/listOptions")
    public R listOptions() {
        return R.success(sysRoleService.listOptions());
    }

    /**
     * 分配菜单
     */
    @PostMapping("/assignMenu")
    @PreAuthorize("@ss.hasPermission('system:sysRole:assignMenu')")
    @Log(title = "角色管理分配菜单")
    public R assignMenu(@Validated @RequestBody SysRoleAssignMenuDTO req) {
        sysRoleService.assignMenu(req);
        return R.success();
    }

    /**
     * 查询角色关联的菜单
     */
    @GetMapping("/listRoleMenus")
    public R listRoleMenus(@RequestParam String roleId) {
        return R.success(sysRoleService.listRoleMenus(roleId));
    }

    /**
     * 查询角色关联的部门
     */
    @GetMapping("/listRoleDepts")
    public R listRoleDepts(@RequestParam String roleId) {
        return R.success(sysRoleService.listRoleDepts(roleId));
    }

    /**
     * 下拉列表
     */
    @GetMapping("/selectOptions")
    public R selectOptions(@RequestParam(required = false) Long tenantId) {
        tenantId = Objects.isNull(tenantId) ? SecurityUtils.getTenantId() : tenantId;
        return R.success(sysRoleService.selectOptions(tenantId));
    }

}

