package com.bluesky.system.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bluesky.common.annotation.Log;
import com.bluesky.common.result.R;
import com.bluesky.system.common.dto.SysLogQueryDTO;
import com.bluesky.system.service.ISysLogService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 * 操作日志表  前端控制器
 * </p>
 *
 * @author Kevin
 * @since 2021-07-12
 */
@RestController
@RequestMapping("/system/sysLog")
public class SysLogController {

    @Resource
    private ISysLogService sysLogService;

    /**
     * 分页查询
     */
    @GetMapping("/page")
    @PreAuthorize("@ss.hasPermission('system:sysLog:page')")
    @Log(title = "操作日志分页查询")
    public R page(Page reqPage, SysLogQueryDTO req) {
        return R.success(sysLogService.page(reqPage, req));
    }

    /**
     * 查看
     */
    @GetMapping("/view")
    @PreAuthorize("@ss.hasPermission('system:sysLog:view')")
    @Log(title = "操作日志查看")
    public R view(@RequestParam String id) {
        return R.success(sysLogService.view(id));
    }

}

