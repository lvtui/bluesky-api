package com.bluesky.system.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bluesky.common.annotation.Log;
import com.bluesky.common.result.R;
import com.bluesky.common.util.ExcelUtils;
import com.bluesky.common.util.SecurityUtils;
import com.bluesky.system.common.dto.SysUserAddDTO;
import com.bluesky.system.common.dto.SysUserEditDTO;
import com.bluesky.system.common.dto.SysUserQueryDTO;
import com.bluesky.system.common.dto.SysUserResetPwdDTO;
import com.bluesky.system.entity.SysUser;
import com.bluesky.system.service.ISysUserService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author Kevin
 * @since 2021-06-10
 */
@RestController
@RequestMapping("/system/sysUser")
public class SysUserController {

    @Resource
    private ISysUserService sysUserService;

    /**
     * 分页查询
     */
    @GetMapping("/page")
    @PreAuthorize("@ss.hasPermission('system:sysUser:page')")
    @Log(title = "用户管理分页查询")
    public R page(Page reqPage, SysUserQueryDTO req) {
        return R.success(sysUserService.page(reqPage, req));
    }

    /**
     * 新增
     */
    @PostMapping("/add")
    @PreAuthorize("@ss.hasPermission('system:sysUser:add')")
    @Log(title = "用户管理新增")
    public R add(@Validated @RequestBody SysUserAddDTO req) {
        if (Objects.isNull(req.getTenantId())) {
            req.setTenantId(SecurityUtils.getTenantId());
        }
        sysUserService.add(req);
        return R.success();
    }

    /**
     * 修改
     */
    @PostMapping("/edit")
    @PreAuthorize("@ss.hasPermission('system:sysUser:edit')")
    @Log(title = "用户管理修改")
    public R edit(@Validated @RequestBody SysUserEditDTO req) {
        sysUserService.edit(req);
        return R.success();
    }

    /**
     * 删除
     */
    @PostMapping("/remove")
    @PreAuthorize("@ss.hasPermission('system:sysUser:remove')")
    @Log(title = "用户管理删除")
    public R remove(@RequestParam String ids) {
        sysUserService.remove(ids);
        return R.success();
    }

    /**
     * 查看
     */
    @GetMapping("/view")
    @PreAuthorize("@ss.hasPermission('system:sysUser:view')")
    @Log(title = "用户管理查看")
    public R view(@RequestParam String id) {
        return R.success(sysUserService.view(id));
    }

    /**
     * 导出
     */
    @GetMapping("/export")
    @PreAuthorize("@ss.hasPermission('system:sysUser:export')")
    @Log(title = "用户管理导出")
    public R export(SysUserQueryDTO req) {
        String filepath = ExcelUtils.export("用户列表", SysUser.class, sysUserService.list(req));
        return R.success(filepath);
    }

    /**
     * 重置密码
     */
    @PostMapping("/resetPwd")
    @PreAuthorize("@ss.hasPermission('system:sysUser:resetPwd')")
    @Log(title = "用户管理导出")
    public R resetPwd(@Validated @RequestBody SysUserResetPwdDTO req) {
        sysUserService.resetPwd(req);
        return R.success();
    }

}

