package com.bluesky.system.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bluesky.common.annotation.Log;
import com.bluesky.common.result.R;
import com.bluesky.common.util.SecurityUtils;
import com.bluesky.system.common.dto.SysDeptAddDTO;
import com.bluesky.system.common.dto.SysDeptEditDTO;
import com.bluesky.system.common.dto.SysDeptQueryDTO;
import com.bluesky.system.service.ISysDeptService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * <p>
 * 部门表 前端控制器
 * </p>
 *
 * @author Kevin
 * @since 2021-06-10
 */
@RestController
@RequestMapping("/system/sysDept")
public class SysDeptController {

    @Resource
    private ISysDeptService sysDeptService;

    /**
     * 分页查询
     */
    @GetMapping("/page")
    @PreAuthorize("@ss.hasPermission('system:sysDept:page')")
    @Log(title = "部门管理分页查询")
    public R page(Page reqPage, SysDeptQueryDTO req) {
        return R.success(sysDeptService.page(reqPage, req));
    }

    /**
     * 新增
     */
    @PostMapping("/add")
    @PreAuthorize("@ss.hasPermission('system:sysDept:add')")
    @Log(title = "部门管理新增")
    public R add(@Validated @RequestBody SysDeptAddDTO req) {
        if (Objects.isNull(req.getTenantId())) {
            req.setTenantId(SecurityUtils.getTenantId());
        }
        sysDeptService.add(req);
        return R.success();
    }

    /**
     * 修改
     */
    @PostMapping("/edit")
    @PreAuthorize("@ss.hasPermission('system:sysDept:edit')")
    @Log(title = "部门管理修改")
    public R edit(@Validated @RequestBody SysDeptEditDTO req) {
        sysDeptService.edit(req);
        return R.success();
    }

    /**
     * 删除
     */
    @PostMapping("/remove")
    @PreAuthorize("@ss.hasPermission('system:sysDept:remove')")
    @Log(title = "部门管理删除")
    public R remove(@RequestParam String ids) {
        sysDeptService.remove(ids);
        return R.success();
    }

    /**
     * 查看
     */
    @GetMapping("/view")
    @PreAuthorize("@ss.hasPermission('system:sysDept:view')")
    @Log(title = "部门管理查看")
    public R view(@RequestParam String id) {
        return R.success(sysDeptService.view(id));
    }

    /**
     * 查询部门树
     */
    @GetMapping("/listDeptTree")
    public R listDeptTree() {
        return R.success(sysDeptService.listDeptTree());
    }

    /**
     * 查询部门树，排除当前节点以及子节点
     */
    @GetMapping("/listDeptTreeAndExcludeNode")
    public R listTreeAndExcludeNode(@RequestParam String nodeId) {
        return R.success(sysDeptService.listDeptTreeAndExcludeNode(nodeId));
    }

    /**
     * 下拉树，排除当前节点以及子节点
     */
    @GetMapping("/selectTreeAndExcludeNode")
    public R selectTreeAndExcludeNode(@RequestParam(required = false) Long tenantId, @RequestParam(required = false) String nodeId) {
        tenantId = Objects.isNull(tenantId) ? SecurityUtils.getTenantId() : tenantId;
        return R.success(sysDeptService.selectTreeAndExcludeNode(tenantId, nodeId));
    }

    /**
     * 下拉树
     */
    @GetMapping("/selectTree")
    public R selectTree(@RequestParam(required = false) Long tenantId) {
        tenantId = Objects.isNull(tenantId) ? SecurityUtils.getTenantId() : tenantId;
        return R.success(sysDeptService.selectTree(tenantId));
    }

}

