package com.bluesky.system.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bluesky.common.annotation.Log;
import com.bluesky.common.result.R;
import com.bluesky.common.util.ExcelUtils;
import com.bluesky.system.common.dto.SysDictItemAddDTO;
import com.bluesky.system.common.dto.SysDictItemEditDTO;
import com.bluesky.system.common.dto.SysDictItemQueryDTO;
import com.bluesky.system.entity.SysDictItem;
import com.bluesky.system.service.ISysDictItemService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 * 字典项 前端控制器
 * </p>
 *
 * @author Kevin
 * @since 2021-06-10
 */
@RestController
@RequestMapping("/system/sysDictItem")
public class SysDictItemController {

    @Resource
    private ISysDictItemService sysDictItemService;

    /**
     * 分页查询
     */
    @GetMapping("/page")
    @PreAuthorize("@ss.hasPermission('system:sysDictItem:page')")
    @Log(title = "字典项管理分页查询")
    public R page(Page reqPage, SysDictItemQueryDTO req) {
        return R.success(sysDictItemService.page(reqPage, req));
    }

    /**
     * 新增
     */
    @PostMapping("/add")
    @PreAuthorize("@ss.hasPermission('system:sysDictItem:add')")
    @Log(title = "字典项管理新增")
    public R add(@Validated @RequestBody SysDictItemAddDTO req) {
        sysDictItemService.add(req);
        return R.success();
    }

    /**
     * 修改
     */
    @PostMapping("/edit")
    @PreAuthorize("@ss.hasPermission('system:sysDictItem:edit')")
    @Log(title = "字典项管理修改")
    public R edit(@Validated @RequestBody SysDictItemEditDTO req) {
        sysDictItemService.edit(req);
        return R.success();
    }

    /**
     * 删除
     */
    @PostMapping("/remove")
    @PreAuthorize("@ss.hasPermission('system:sysDictItem:remove')")
    @Log(title = "字典项管理删除")
    public R remove(@RequestParam String ids) {
        sysDictItemService.remove(ids);
        return R.success();
    }

    /**
     * 查看
     */
    @GetMapping("/view")
    @PreAuthorize("@ss.hasPermission('system:sysDictItem:view')")
    @Log(title = "字典项管理查看")
    public R view(@RequestParam String id) {
        return R.success(sysDictItemService.view(id));
    }

    /**
     * 导出
     */
    @GetMapping("/export")
    @PreAuthorize("@ss.hasPermission('system:sysDictItem:export')")
    @Log(title = "字典项管理导出")
    public R export(SysDictItemQueryDTO req) {
        String filepath = ExcelUtils.export("字典项列表", SysDictItem.class, sysDictItemService.list(req));
        return R.success(filepath);
    }

}

