package com.bluesky.framework.security.service.impl;

import cn.hutool.core.util.StrUtil;
import com.bluesky.common.bo.LoginUser;
import com.bluesky.common.util.SecurityUtils;
import com.bluesky.framework.security.service.IPermissionService;
import org.springframework.stereotype.Service;
import org.springframework.util.PatternMatchUtils;

/**
 * 权限实现类
 *
 * @author Kevin
 */
@Service("ss")
public class PermissionServiceImpl implements IPermissionService {

    @Override
    public boolean hasPermission(String permission) {
        if (StrUtil.isBlank(permission)) {
            return false;
        }
        LoginUser loginUser = SecurityUtils.getLoginUser();
        // 如果是超级管理员，具有所有权限
        if (SecurityUtils.isSuperAdmin()) {
            return true;
        }
        return loginUser.getPermissions().stream().anyMatch(item -> PatternMatchUtils.simpleMatch(permission, item));
    }
}
