package com.bluesky.framework.security.handler;

import cn.hutool.core.util.CharsetUtil;
import cn.hutool.extra.servlet.ServletUtil;
import cn.hutool.http.ContentType;
import cn.hutool.json.JSONUtil;
import com.bluesky.common.result.R;
import com.bluesky.common.result.ResultCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 无权访问处理器
 *
 * @author Kevin
 */
@Component
@Slf4j
public class CustomAccessDeniedHandler implements AccessDeniedHandler {

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException e) throws IOException, ServletException {
        e.printStackTrace();
        log.info(ResultCode.UN_AUTHORIZED.getMessage() + "，" + e.getMessage());

        response.setCharacterEncoding(CharsetUtil.UTF_8);
        ServletUtil.write(response, JSONUtil.toJsonStr(R.result(ResultCode.UN_AUTHORIZED)), ContentType.JSON.toString());
    }

}
