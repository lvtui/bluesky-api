package com.bluesky.framework.security.config;

import lombok.extern.slf4j.Slf4j;

/**
 * 多租户
 *
 * @author Kevin
 */
@Slf4j
public class TenantContextHolder {

    /**
     * 使用ThreadLocal维护变量，ThreadLocal为每个使用该变量的线程提供独立的变量副本，
     * 所以每一个线程都可以独立地改变自己的副本，而不会影响其它线程所对应的副本。
     */
    private static final ThreadLocal<String> CONTEXT_HOLDER = new ThreadLocal<>();

    /**
     * 获得当前租户
     */
    public static String getCurrentTenant() {
        return CONTEXT_HOLDER.get();
    }

    /**
     * 设置当前租户
     */
    public static void setCurrentTenant(String tenant) {
        CONTEXT_HOLDER.set(tenant);
    }
}