package com.bluesky.framework.security.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.bluesky.common.bo.LoginUser;
import com.bluesky.common.bo.SysRoleBO;
import com.bluesky.common.bo.SysUserBO;
import com.bluesky.common.enums.*;
import com.bluesky.common.exception.CustomException;
import com.bluesky.common.util.SecurityUtils;
import com.bluesky.framework.security.config.TenantContextHolder;
import com.bluesky.framework.security.service.ICustomUserDetailsService;
import com.bluesky.system.entity.SysRole;
import com.bluesky.system.entity.SysTenant;
import com.bluesky.system.entity.SysUser;
import com.bluesky.system.service.ISysMenuService;
import com.bluesky.system.service.ISysRoleService;
import com.bluesky.system.service.ISysTenantService;
import com.bluesky.system.service.ISysUserService;
import com.google.common.collect.Sets;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 自定义UserDetails
 *
 * @author Kevin
 */
@Service
@Slf4j
public class CustomUserDetailsServiceImpl implements ICustomUserDetailsService {

    @Resource
    private ISysUserService sysUserService;

    @Resource
    private ISysRoleService sysRoleService;

    @Resource
    private ISysMenuService sysMenuService;

    @Resource
    private ISysTenantService sysTenantService;

    @Override
    public UserDetails loadUserByMobile(String mobile) throws UsernameNotFoundException {
        SysTenant sysTenant = sysTenantService.getOne(Wrappers.lambdaQuery(SysTenant.class).eq(SysTenant::getTenantCode, TenantContextHolder.getCurrentTenant()));
        if (Objects.isNull(sysTenant)) {
            throw new CustomException("租户未找到");
        }
        if (sysTenant.getStatus().equals(StatusEnum.NO.getCode())) {
            throw new CustomException("租户已停用");
        }
        if (sysTenant.getTenantPeriod().equals(PeriodEnum.SHORT_PERIOD.getCode())) {
            if (DateUtil.compare(new Date(), DateUtil.offsetDay(sysTenant.getEndDate(), 1)) > 0) {
                throw new CustomException("租户已过期");
            }
        }
        SysUser sysUser = sysUserService.getOne(Wrappers.lambdaQuery(SysUser.class).eq(SysUser::getPhone, mobile).eq(SysUser::getTenantId, sysTenant.getId()));
        if (Objects.isNull(sysUser)) {
            log.info("登录用户：{}不存在", mobile);
            throw new UsernameNotFoundException("登录用户不存在");
        }
        if (sysUser.getDelFlag().equals(DelFlagEnum.YES.getCode())) {
            log.info("登录用户：{}已被删除", mobile);
            throw new CustomException("对不起，您的账号已被删除");
        }
        if (sysUser.getStatus().equals(StatusEnum.NO.getCode())) {
            log.info("登录用户：{}已被停用", mobile);
            throw new CustomException("对不起，您的账号已被停用");
        }

        SysUserBO sysUserBO = BeanUtil.copyProperties(sysUser, SysUserBO.class);

        // 查询角色列表
        List<SysRole> sysRoleList = sysRoleService.listSysRoleByUserId(sysUser.getId());

        // 设置角色列表
        sysUserBO.setRoles(sysRoleList.stream().map(item -> BeanUtil.copyProperties(item, SysRoleBO.class)).collect(Collectors.toList()));

        // 查询权限标识
        Set<String> permissions = Sets.newHashSet();
        if (SecurityUtils.isSuperAdmin(sysUser.getId())) {
            permissions.add("*:*:*");
        } else {
            permissions = sysMenuService.getPermissionsByUserId(sysUser.getId());
        }

        LoginUser loginUser = new LoginUser(UserPlatformEnum.WEB.getCode(), GrantTypeEnum.MOBILE_CODE.getCode(), sysUserBO, mobile, "", permissions);
        loginUser.setTenantId(sysTenant.getId());
        loginUser.setTenantCode(sysTenant.getTenantCode());
        loginUser.setTenantName(sysTenant.getTenantName());
        return loginUser;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        SysTenant sysTenant = sysTenantService.getOne(Wrappers.lambdaQuery(SysTenant.class).eq(SysTenant::getTenantCode, TenantContextHolder.getCurrentTenant()));
        if (Objects.isNull(sysTenant)) {
            throw new CustomException("租户未找到");
        }
        if (sysTenant.getStatus().equals(StatusEnum.NO.getCode())) {
            throw new CustomException("租户已停用");
        }
        if (sysTenant.getTenantPeriod().equals(PeriodEnum.SHORT_PERIOD.getCode())) {
            if (DateUtil.compare(new Date(), DateUtil.offsetDay(sysTenant.getEndDate(), 1)) > 0) {
                throw new CustomException("租户已过期");
            }
        }
        SysUser sysUser = sysUserService.getOne(Wrappers.lambdaQuery(SysUser.class).eq(SysUser::getAccount, username).eq(SysUser::getTenantId, sysTenant.getId()));
        if (Objects.isNull(sysUser)) {
            log.info("登录用户：{}不存在", username);
            throw new UsernameNotFoundException("登录用户不存在");
        }
        if (sysUser.getDelFlag().equals(DelFlagEnum.YES.getCode())) {
            log.info("登录用户：{}已被删除", username);
            throw new CustomException("对不起，您的账号已被删除");
        }
        if (sysUser.getStatus().equals(StatusEnum.NO.getCode())) {
            log.info("登录用户：{}已被停用", username);
            throw new CustomException("对不起，您的账号已被停用");
        }

        SysUserBO sysUserBO = BeanUtil.copyProperties(sysUser, SysUserBO.class);

        // 查询角色列表
        List<SysRole> sysRoleList = sysRoleService.listSysRoleByUserId(sysUser.getId());

        // 设置角色列表
        sysUserBO.setRoles(sysRoleList.stream().map(item -> BeanUtil.copyProperties(item, SysRoleBO.class)).collect(Collectors.toList()));

        // 查询权限标识
        Set<String> permissions = Sets.newHashSet();
        if (SecurityUtils.isSuperAdmin(sysUser.getId())) {
            permissions.add("*:*:*");
        } else {
            permissions = sysMenuService.getPermissionsByUserId(sysUser.getId());
        }

        LoginUser loginUser = new LoginUser(UserPlatformEnum.WEB.getCode(), GrantTypeEnum.USERNAME_PASSWORD.getCode(), sysUserBO, username, sysUser.getPassword(), permissions);
        loginUser.setTenantId(sysTenant.getId());
        loginUser.setTenantCode(sysTenant.getTenantCode());
        loginUser.setTenantName(sysTenant.getTenantName());
        return loginUser;
    }

}
