package com.bluesky.framework.aspect;

import cn.hutool.core.thread.ThreadUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.spring.SpringUtil;
import cn.hutool.http.useragent.UserAgent;
import cn.hutool.http.useragent.UserAgentUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bluesky.common.Constants;
import com.bluesky.common.annotation.Log;
import com.bluesky.common.bo.LoginUser;
import com.bluesky.common.enums.LogStatusEnum;
import com.bluesky.common.util.AddressUtils;
import com.bluesky.common.util.IpUtils;
import com.bluesky.framework.web.service.ITokenService;
import com.bluesky.system.entity.SysLog;
import com.bluesky.system.service.ISysLogService;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 日志注解
 *
 * @author Kevin
 */
@Aspect
@Component
@Slf4j
public class LogAspect {

    @Pointcut("@annotation(com.bluesky.common.annotation.Log)")
    public void logPointCut() {
    }

    @Around("logPointCut()")
    @SneakyThrows
    public Object around(ProceedingJoinPoint point) {
        Long startTime = System.currentTimeMillis();
        Object obj = null;
        Exception exception = null;
        try {
            obj = point.proceed();
        } catch (Exception e) {
            exception = e;
            throw e;
        } finally {
            Long endTime = System.currentTimeMillis();
            handleLog(point, exception, obj, endTime - startTime);
        }
        return obj;
    }

    protected void handleLog(final JoinPoint joinPoint, final Exception e, Object retValue, long time) {
        try {
            // 获得注解
            Signature signature = joinPoint.getSignature();
            MethodSignature methodSignature = (MethodSignature) signature;
            Log controllerLog = AnnotationUtils.findAnnotation(methodSignature.getMethod(), Log.class);
            if (Objects.isNull(controllerLog)) {
                return;
            }
            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
            SysLog sysLog = new SysLog();
            sysLog.setTitle(controllerLog.title());
            sysLog.setLogStatus(LogStatusEnum.SUCCESS.getCode());
            sysLog.setUserPlatform(controllerLog.userPlatform().getCode());
            sysLog.setRequsetUri(request.getRequestURI());
            sysLog.setRequsetType(request.getMethod());
            // 设置方法名称
            String className = joinPoint.getTarget().getClass().getName();
            String methodName = joinPoint.getSignature().getName();
            sysLog.setRequsetMethod(className + "." + methodName + "()");

            List<Object> objectList = Arrays.stream(joinPoint.getArgs()).filter(item -> Objects.nonNull(item) && !isFilterObject(item)).collect(Collectors.toList());
            sysLog.setRequsetParams(JSONUtil.toJsonStr(objectList));
            sysLog.setResponseResult(JSONUtil.toJsonStr(retValue));
            sysLog.setRequsetTime(String.valueOf(time));

            String ipAddress = IpUtils.getClientIP(request);
            sysLog.setIpAddress(ipAddress);
            sysLog.setOperLocation(AddressUtils.getRealAddressByIp(ipAddress));
            UserAgent userAgent = UserAgentUtil.parse(request.getHeader("User-Agent"));
            sysLog.setBrowser(userAgent.getBrowser().getName());
            sysLog.setOs(userAgent.getOs().getName());

            String token = request.getHeader(Constants.HEADER_TOKEN);
            if (StrUtil.isNotBlank(token)) {
                LoginUser loginUser = SpringUtil.getBean(ITokenService.class).getUserByToken(token);
                sysLog.setTenantId(loginUser.getTenantId());
                sysLog.setOperName(loginUser.getUsername());
            }
            if (e != null) {
                sysLog.setLogStatus(LogStatusEnum.FAILURE.getCode());
                sysLog.setException(e.getMessage());
            }
            ThreadUtil.execAsync(() -> {
                ISysLogService sysLogService = SpringUtil.getBean(ISysLogService.class);
                sysLogService.save(sysLog);
            });
            log.debug("接口：{}，URI：{}，执行耗时：{}", sysLog.getTitle(), sysLog.getRequsetUri(), time >= 1000 ? time / 1000 + "s" : time + "ms");
        } catch (Exception exception) {
            log.error("异常信息:{}", exception.getMessage());
            exception.printStackTrace();
        }
    }

    /**
     * 判断是否需要过滤的对象。
     *
     * @param o 对象信息。
     * @return 如果是需要过滤的对象，则返回true；否则返回false。
     */
    public boolean isFilterObject(final Object o) {
        Class<?> clazz = o.getClass();
        if (clazz.isArray()) {
            return clazz.getComponentType().isAssignableFrom(MultipartFile.class);
        } else if (Collection.class.isAssignableFrom(clazz)) {
            Collection collection = (Collection) o;
            for (Object value : collection) {
                return value instanceof MultipartFile;
            }
        } else if (Map.class.isAssignableFrom(clazz)) {
            Map map = (Map) o;
            for (Object value : map.entrySet()) {
                Map.Entry entry = (Map.Entry) value;
                return entry.getValue() instanceof MultipartFile;
            }
        }
        return o instanceof MultipartFile || o instanceof HttpServletRequest || o instanceof HttpServletResponse
                || o instanceof BindingResult || o instanceof Page;
    }
}
