package com.bluesky.framework.web.service;

import com.bluesky.common.dto.LoginDTO;
import com.bluesky.system.common.vo.AccountInfoVO;
import com.bluesky.system.common.vo.RouteItemVO;
import com.bluesky.system.common.vo.UserInfoVO;

import java.util.List;
import java.util.Set;

/**
 * 用户接口
 *
 * @author Kevin
 */
public interface IUserService {

    /**
     * 登录接口
     *
     * @param req
     * @return
     */
    String login(LoginDTO req);

    /**
     * 获取用户信息
     *
     * @return
     */
    UserInfoVO getUserInfo();

    /**
     * 获取权限标识
     *
     * @return
     */
    Set<String> getPermCode();

    /**
     * 获取菜单列表
     *
     * @return
     */
    List<RouteItemVO> getMenuList();

    /**
     * 获取账户信息
     *
     * @return
     */
    AccountInfoVO getAccountInfo();

    /**
     * 更新账户信息
     *
     * @return
     */
    void saveAccountInfo(AccountInfoVO req);

}
